IRC Log Backup
==============

This is a backup of [this chat log] from the [`#foot` IRC channel],
which can be used in place of the link in `README.md` if whitequark.org
goes offline.

```
20:37 <cbb> dnkl: is it reliable to assume foot will send at least 2x SIGWINCH at startup on sway?
20:37 <cbb> ...or in other words is it safe to change waitwinch to wait for 2 SIGWINCH signals with no timeout?
20:39 <dnkl> cbb: I _think_ so. But probably only if the new foot window isn't floating
20:39 <dnkl> and it's been a while since I tested this on Sway
20:39 <dnkl> should be easy to find out though; just run foot with WAYLAND_DEBUG=1 and check for "configure" events
20:40 <cbb> dnkl: ah nice, thanks for the pointers
20:40 <cbb> it seems to work at least
20:40 <cbb> including with mtm
20:41 <dnkl> cbb: also seeing the same events on river (three xdg_toplevel.configure events: 0x0, 700x500, and the final, tiled, size)
20:42 <dnkl> nice!
20:42 <cbb> dnkl: does that imply there's 3 resizes on river?
20:43 <cbb> so 3x SIGWINCH?
20:43 <dnkl> no, two. First one is before we're mapped, asking us to set a size (that's the 0x0 event). We create a surface that's 700x500, and river replies with a corresponding configure event. Then river tiles us, and we get last configure event
20:43 <dnkl> this matches Sway's behavior
20:44 <dnkl> as I remmeber it
20:44 <cbb> ah ok, that's good to know
20:44 <dnkl> cbb: we send a SIGWINCH on the second and third configure events
20:44 <cbb> oh I see, yeah that makes more sense now
20:44 <dnkl> before the first SIGWINCH, we set a TIOCSWINSIZE=80x24
20:45 <dnkl> so, the client application *can* see three different sizes
20:45 <dnkl> but only two SIGWINCHes
20:46 <dnkl> (we used to have an initial WINSZ of 0x0, but that triggered crashes in some shells...)
20:46 <emersion> this is a bug, fwiw
20:46 <emersion> sway should send the correct size ideally, instead of 0x0
20:47 <cbb> emersion: I'm guessing there's some complexities to fixing it?
20:47 <dnkl> emersion: someone asked on the issue tracker if this behavior _was_ a bug. Good to know it is. Is there a bug report?
20:47 <emersion> https://github.com/swaywm/sway/issues/2176
20:48 <dnkl> thanks!
20:48 <emersion> cbb: it's not trivial, but shouldn't be that hard
```


[this chat log]: https://libera.irclog.whitequark.org/foot/2022-02-02#1643834221-1643834796;
[`#foot` IRC channel]: https://codeberg.org/dnkl/foot#irc
