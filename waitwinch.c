#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

enum {
    NS_PER_SECOND = 1000000000L,
    MS_PER_SECOND = 1000L,
    NS_PER_MS = 1000000L,
};

// Note that this timeout can potentially be doubled, if waiting for
// 2 SIGWINCH signals. It's not intended to be precise; it's simply
// a way to avoid deadlocking if the expected signals never arrive.
static const struct timespec timeout = {
    .tv_sec = 0,
    .tv_nsec = 150L * NS_PER_MS,
};

static struct timespec get_time(void)
{
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts) != 0) {
        perror("clock_gettime");
        exit(1);
    }
    return ts;
}

static unsigned long timespec_diff_ms (
    const struct timespec *start,
    const struct timespec *end
) {
    assert(end->tv_nsec < NS_PER_SECOND && start->tv_nsec < NS_PER_SECOND);
    long nsec = end->tv_nsec - start->tv_nsec;
    time_t sec = (end->tv_sec - start->tv_sec) - (nsec < 0);
    assert(sec >= 0);
    nsec += (nsec < 0 ? NS_PER_SECOND : 0);
    return (sec * MS_PER_SECOND) + (nsec / NS_PER_MS);
}

int main(int argc, char *argv[])
{
    // Block SIGWINCH as early as possible
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGWINCH);
    sigprocmask(SIG_BLOCK, &set, NULL);

    bool timing = false;
    unsigned int winch_count = 2;
    for (int ch; (ch = getopt(argc, argv, "1v")) != -1; ) {
        switch (ch) {
        case '1':
            winch_count = 1;
            break;
        case 'v':
            timing = true;
            break;
        default:
            return 1;
        }
    }

    if (optind >= argc) {
        fprintf(stderr, "Usage: %s [-1] COMMAND [ARGS]...\n", argv[0]);
        return 1;
    }

    struct timespec start;
    if (timing) {
        start = get_time();
    }

    for (unsigned int i = 0; i < winch_count; i++) {
        int r = sigtimedwait(&set, NULL, &timeout);
        if (r == -1) {
            if (errno == EAGAIN) {
                fputs("waitwinch: timeout expired\n", stderr);
                timing = false;
                break;
            }
            perror("sigtimedwait");
            return 1;
        }
        assert(r == SIGWINCH);
    }

    if (timing) {
        struct timespec end = get_time();
        unsigned long ms = timespec_diff_ms(&start, &end);
        fprintf(stderr, "waitwinch: waited %lums for %ux SIGWINCH\n", ms, winch_count);
    }

    // Unblock SIGWINCH
    sigprocmask(SIG_UNBLOCK, &set, NULL);

    // Exec specified command
    execvp(argv[optind], argv + optind);
    perror("execvp");
    return 1;
}
