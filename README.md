waitwinch
=========

This utility waits for the arrival of 2 `SIGWINCH` signals and then
executes the specified command.

Rather than trying to explain the intended use case here, I'll simply
link to [foot] issues [#904] and [#453] and [this IRC log].

Like the `sleep 0.1` workaround suggested in the issues above, this is
somewhat of a hack. However, the benefit of using this method instead of
a fixed delay is that it should keep the startup latency to a minimum.

Usage Examples
--------------

```sh
# Run an app in foot, after first waiting for the arrival of 2 SIGWINCH
# signals (should work in Sway and River)
foot waitwinch tui-app args...

# Run demonstration program in foot, both with and without waitwinch
# (5 is the default number of milliseconds between printing each line)
foot --hold ./waitwinch ./wwdemo 5
foot --hold ./wwdemo 5

# As above, but also printing timing info (-v)
foot --hold ./waitwinch -v ./wwdemo 5
```

*Note*: if `SIGWINCH` doesn't arrive within 150ms, a timeout will expire
and the command will be executed anyway. This is to avoid deadlocks in
case something unexpected happens.

Usage in [`foot.ini`]
---------------------

```ini
# $(command -v ...) is used here so that $SHELL is executed directly
# if waitwinch isn't available in $PATH for whatever reason
shell = sh -c 'exec $(command -v waitwinch) ${SHELL:-sh}'
```

Installing
----------

First ensure you have a C99 compiler and GNU Make installed, then run
the following commands:

```sh
git clone https://codeberg.org/craigbarnes/waitwinch.git
cd waitwinch
make && sudo make install
```

License
-------

[Unlicense](UNLICENSE)


[foot]: https://codeberg.org/dnkl/foot
[#904]: https://codeberg.org/dnkl/foot/issues/904
[#453]: https://codeberg.org/dnkl/foot/issues/453
[this IRC log]: https://libera.irclog.whitequark.org/foot/2022-02-02#1643834221-1643834796;
[`foot.ini`]: https://codeberg.org/dnkl/foot/src/commit/22e1b1610f622ca9e2a7e64593025a8d44bc7b14/doc/foot.ini.5.scd#L36-L40
