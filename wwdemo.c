#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

#define STRLEN(x) (sizeof("" x "") - 1)
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define NS_PER_MS (1000000L)

static void print_term_size(void)
{
    // Get the terminal size
    struct winsize ws;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1) {
        perror("TIOCGWINSZ");
        exit(1);
    }

    // Emit SGR 7, to reverse the fg/bg color
    fputs("\033[0;7m", stdout);

    // Print `w x h ` (with a trailing space, to be reapeated)
    unsigned int w = ws.ws_col, h = ws.ws_row;
    int text_width = fprintf(stdout, "%u x %u ", w, h);
    if (text_width < 0) {
        perror("fprintf");
        exit(1);
    }

    // Emit REP, with a parameter such that `w` columns are filled
    unsigned int rep_count = w - MIN(w, (unsigned int)text_width);
    fprintf(stdout, "\033[%ub\033[0m\n", rep_count);
}

int main(int argc, char *argv[])
{
    // Print the first line immediately
    print_term_size();

    // Use argv[1] as a delay value, or default to 5ms
    long nsec = 5 * NS_PER_MS;
    if (argc >= 2 && argv[1][0] != '\0') {
        const char *str = argv[1];
        char *end;
        long ms = strtol(str, &end, 10);
        if (ms < 1 || ms > 999 || end[0] != '\0') {
            fprintf(stderr, "Error: invalid delay '%s' (expected 1-999)\n", str);
            return 1;
        }
        nsec = ms * NS_PER_MS;
    }

    // Print 22 more lines, with a delay between each
    const struct timespec delay = {.tv_nsec = nsec};
    for (unsigned int i = 0; i < 22; i++) {
        print_term_size();
        nanosleep(&delay, NULL);
    }

    return 0;
}
